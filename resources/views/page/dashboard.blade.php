@extends('layout.base')
@section('content')
<div class="page-wrapper" style="min-height: 534px;">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">Dashboard 1</h4>
            </div>
            <div class="col-md-7 align-self-center text-end">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb justify-content-end">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard 1</li>
                    </ol>
                    <button type="button" class="btn btn-info d-none d-lg-block m-l-15 text-white"><i
                            class="fa fa-plus-circle"></i> Create New</button>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Info box -->
        <!-- ============================================================== -->
        <!-- Row -->
        <div class="row">
            <!-- Column -->
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex flex-row">
                            <div class="round align-self-center round-success"><i class="ti-wallet"></i></div>
                            <div class="m-l-10 align-self-center">
                                <h3 class="m-b-0">3564</h3>
                                <h5 class="text-muted m-b-0">New Customers</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex flex-row">
                            <div class="round align-self-center round-info"><i class="ti-user"></i></div>
                            <div class="m-l-10 align-self-center">
                                <h3 class="m-b-0">342</h3>
                                <h5 class="text-muted m-b-0">New Products</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex flex-row">
                            <div class="round align-self-center round-danger"><i class="ti-calendar"></i></div>
                            <div class="m-l-10 align-self-center">
                                <h3 class="m-b-0">56%</h3>
                                <h5 class="text-muted m-b-0">Today's Profit</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-lg-3 col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex flex-row">
                            <div class="round align-self-center round-success"><i class="ti-settings"></i></div>
                            <div class="m-l-10 align-self-center">
                                <h3 class="m-b-0">56%</h3>
                                <h5 class="text-muted m-b-0">New Leads</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Column -->
        </div>
        <!-- Row -->
        <!-- ============================================================== -->
        <!-- End Info box -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Over Visitor, Our income , slaes different and  sales prediction -->
        <!-- ============================================================== -->
        <div class="row">
            <!-- Column -->
            <div class="col-lg-4 col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title ">Leads by Source</h5>
                        <div id="morris-donut-chart" class="ecomm-donute" style="height: 317px;"><svg height="317"
                                version="1.1" width="491.656" xmlns="http://www.w3.org/2000/svg"
                                xmlns:xlink="http://www.w3.org/1999/xlink"
                                style="overflow: hidden; position: relative;">
                                <desc style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created with Raphaël 2.2.0
                                </desc>
                                <defs style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs>
                                <path fill="none" stroke="#fb9678"
                                    d="M245.828,257.5A99,99,0,0,0,248.3432801989657,59.531957857494774" stroke-width="2"
                                    opacity="0" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); opacity: 0;">
                                </path>
                                <path fill="#fb9678" stroke="#ffffff"
                                    d="M245.828,260.5A102,102,0,0,0,248.41950081105557,56.53292627741885L249.47388594496545,15.046322753035355A143.5,143.5,0,0,1,245.828,302Z"
                                    stroke-width="3" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
                                <path fill="none" stroke="#01c0c8"
                                    d="M248.3432801989657,59.531957857494774A99,99,0,0,0,149.58320999241113,135.30645787303888"
                                    stroke-width="2" opacity="0"
                                    style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); opacity: 0;"></path>
                                <path fill="#01c0c8" stroke="#ffffff"
                                    d="M248.41950081105557,56.53292627741885A102,102,0,0,0,146.66670120430237,134.60362326313097L106.32166296879794,124.8810778260715A143.5,143.5,0,0,1,249.47388594496545,15.046322753035355Z"
                                    stroke-width="3" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
                                <path fill="none" stroke="#4f5467"
                                    d="M149.58320999241113,135.30645787303888A99,99,0,0,0,245.79689823324108,257.49999511454587"
                                    stroke-width="2" opacity="1"
                                    style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); opacity: 1;"></path>
                                <path fill="#4f5467" stroke="#ffffff"
                                    d="M146.66670120430237,134.60362326313097A102,102,0,0,0,245.7959557554605,260.4999949665018L245.78134734986162,306.9999926718188A148.5,148.5,0,0,1,101.46081498861668,123.70968680955832Z"
                                    stroke-width="3" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text
                                    x="245.828" y="148.5" text-anchor="middle" font-family="&quot;Arial&quot;"
                                    font-size="15px" stroke="none" fill="#000000"
                                    style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: Arial; font-size: 15px; font-weight: 800;"
                                    font-weight="800" transform="matrix(2.9118,0,0,2.9118,-469.9655,-300.1471)"
                                    stroke-width="0.3434343434343434">
                                    <tspan dy="5.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Web</tspan>
                                </text><text x="245.828" y="168.5" text-anchor="middle" font-family="&quot;Arial&quot;"
                                    font-size="14px" stroke="none" fill="#000000"
                                    style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: Arial; font-size: 14px;"
                                    transform="matrix(2.0625,0,0,2.0625,-261.3003,-170.5313)"
                                    stroke-width="0.48484848484848486">
                                    <tspan dy="5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">4,870</tspan>
                                </text>
                            </svg></div>
                        <ul class="list-inline m-t-30 text-center">
                            <li class="p-r-20">
                                <h5 class="text-muted"><i class="fa fa-circle" style="color: #fb9678;"></i> Ads</h5>
                                <h4 class="m-b-0">8500</h4>
                            </li>
                            <li class="p-r-20">
                                <h5 class="text-muted"><i class="fa fa-circle" style="color: #01c0c8;"></i> Tredshow
                                </h5>
                                <h4 class="m-b-0">3630</h4>
                            </li>
                            <li>
                                <h5 class="text-muted"> <i class="fa fa-circle" style="color: #4F5467;"></i> Web</h5>
                                <h4 class="m-b-0">4870</h4>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <div class="col-lg-8 col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Top Products sales</h5>
                        <ul class="list-inline text-center">
                            <li>
                                <h5><i class="fa fa-circle m-r-5" style="color: #00bfc7;"></i>iMac</h5>
                            </li>
                            <li>
                                <h5><i class="fa fa-circle m-r-5" style="color: #b4becb;"></i>iPhone</h5>
                            </li>
                        </ul>
                        <div id="morris-area-chart2"
                            style="height: 370px; position: relative; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                            <svg height="370" version="1.1" width="1043.33" xmlns="http://www.w3.org/2000/svg"
                                xmlns:xlink="http://www.w3.org/1999/xlink"
                                style="overflow: hidden; position: relative; left: -0.65625px;">
                                <desc style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created with Raphaël 2.2.0
                                </desc>
                                <defs style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs><text x="32.859375"
                                    y="331" text-anchor="end" font-family="sans-serif" font-size="12px" stroke="none"
                                    fill="#888888"
                                    style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;"
                                    font-weight="normal">
                                    <tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">0</tspan>
                                </text>
                                <path fill="none" stroke="#e0e0e0" d="M45.359375,331H1018.3299999999999"
                                    stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
                                <text x="32.859375" y="254.5" text-anchor="end" font-family="sans-serif"
                                    font-size="12px" stroke="none" fill="#888888"
                                    style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;"
                                    font-weight="normal">
                                    <tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">75</tspan>
                                </text>
                                <path fill="none" stroke="#e0e0e0" d="M45.359375,254.5H1018.3299999999999"
                                    stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
                                <text x="32.859375" y="178" text-anchor="end" font-family="sans-serif" font-size="12px"
                                    stroke="none" fill="#888888"
                                    style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;"
                                    font-weight="normal">
                                    <tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">150</tspan>
                                </text>
                                <path fill="none" stroke="#e0e0e0" d="M45.359375,178H1018.3299999999999"
                                    stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
                                <text x="32.859375" y="101.5" text-anchor="end" font-family="sans-serif"
                                    font-size="12px" stroke="none" fill="#888888"
                                    style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;"
                                    font-weight="normal">
                                    <tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">225</tspan>
                                </text>
                                <path fill="none" stroke="#e0e0e0" d="M45.359375,101.5H1018.3299999999999"
                                    stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
                                <text x="32.859375" y="25" text-anchor="end" font-family="sans-serif" font-size="12px"
                                    stroke="none" fill="#888888"
                                    style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;"
                                    font-weight="normal">
                                    <tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">300</tspan>
                                </text>
                                <path fill="none" stroke="#e0e0e0" d="M45.359375,25H1018.3299999999999"
                                    stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
                                <text x="1018.3299999999999" y="343.5" text-anchor="middle" font-family="sans-serif"
                                    font-size="12px" stroke="none" fill="#888888"
                                    style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;"
                                    font-weight="normal" transform="matrix(1,0,0,1,0,7)">
                                    <tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2016</tspan>
                                </text><text x="856.2422418416247" y="343.5" text-anchor="middle"
                                    font-family="sans-serif" font-size="12px" stroke="none" fill="#888888"
                                    style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;"
                                    font-weight="normal" transform="matrix(1,0,0,1,0,7)">
                                    <tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2015</tspan>
                                </text><text x="694.1544836832496" y="343.5" text-anchor="middle"
                                    font-family="sans-serif" font-size="12px" stroke="none" fill="#888888"
                                    style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;"
                                    font-weight="normal" transform="matrix(1,0,0,1,0,7)">
                                    <tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2014</tspan>
                                </text><text x="532.0667255248745" y="343.5" text-anchor="middle"
                                    font-family="sans-serif" font-size="12px" stroke="none" fill="#888888"
                                    style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;"
                                    font-weight="normal" transform="matrix(1,0,0,1,0,7)">
                                    <tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2013</tspan>
                                </text><text x="369.5348913167503" y="343.5" text-anchor="middle"
                                    font-family="sans-serif" font-size="12px" stroke="none" fill="#888888"
                                    style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;"
                                    font-weight="normal" transform="matrix(1,0,0,1,0,7)">
                                    <tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2012</tspan>
                                </text><text x="207.44713315837515" y="343.5" text-anchor="middle"
                                    font-family="sans-serif" font-size="12px" stroke="none" fill="#888888"
                                    style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;"
                                    font-weight="normal" transform="matrix(1,0,0,1,0,7)">
                                    <tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2011</tspan>
                                </text><text x="45.359375" y="343.5" text-anchor="middle" font-family="sans-serif"
                                    font-size="12px" stroke="none" fill="#888888"
                                    style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: middle; font-family: sans-serif; font-size: 12px; font-weight: normal;"
                                    font-weight="normal" transform="matrix(1,0,0,1,0,7)">
                                    <tspan dy="4" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2010</tspan>
                                </text>
                                <path fill="#e2e5ea" stroke="none"
                                    d="M45.359375,331C85.88131453959379,297.85,166.92519361878135,202.225,207.44713315837515,198.4C247.96907269796895,194.57500000000002,329.0129517771565,287.6674418604651,369.5348913167503,300.4C410.16784986878133,313.1674418604651,491.4337669728435,322.1046511627907,532.0667255248745,300.4C572.5886650644683,278.7546511627907,653.6325441436559,136.5625,694.1544836832496,127C734.6764232228434,117.4375,815.720302302031,230.27499999999998,856.2422418416247,223.89999999999998C896.7641813812186,217.52499999999998,977.8080604604061,112.975,1018.3299999999999,76L1018.3299999999999,331L45.359375,331Z"
                                    fill-opacity="0.4"
                                    style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 0.4;"></path>
                                <path fill="none" stroke="#b4becb"
                                    d="M45.359375,331C85.88131453959379,297.85,166.92519361878135,202.225,207.44713315837515,198.4C247.96907269796895,194.57500000000002,329.0129517771565,287.6674418604651,369.5348913167503,300.4C410.16784986878133,313.1674418604651,491.4337669728435,322.1046511627907,532.0667255248745,300.4C572.5886650644683,278.7546511627907,653.6325441436559,136.5625,694.1544836832496,127C734.6764232228434,117.4375,815.720302302031,230.27499999999998,856.2422418416247,223.89999999999998C896.7641813812186,217.52499999999998,977.8080604604061,112.975,1018.3299999999999,76"
                                    stroke-width="0" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
                                <circle cx="45.359375" cy="331" r="0" fill="#b4becb" stroke="#b4becb" stroke-width="1"
                                    style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                <circle cx="207.44713315837515" cy="198.4" r="0" fill="#b4becb" stroke="#b4becb"
                                    stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                <circle cx="369.5348913167503" cy="300.4" r="0" fill="#b4becb" stroke="#b4becb"
                                    stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                <circle cx="532.0667255248745" cy="300.4" r="0" fill="#b4becb" stroke="#b4becb"
                                    stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                <circle cx="694.1544836832496" cy="127" r="0" fill="#b4becb" stroke="#b4becb"
                                    stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                <circle cx="856.2422418416247" cy="223.89999999999998" r="0" fill="#b4becb"
                                    stroke="#b4becb" stroke-width="1"
                                    style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                <circle cx="1018.3299999999999" cy="76" r="0" fill="#b4becb" stroke="#b4becb"
                                    stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                <path fill="#0ddbe4" stroke="none"
                                    d="M45.359375,331C85.88131453959379,305.5,166.92519361878135,236.65,207.44713315837515,229C247.96907269796895,221.35,329.0129517771565,282.5325581395349,369.5348913167503,269.8C410.16784986878133,257.0325581395349,491.4337669728435,138.4906976744186,532.0667255248745,127C572.5886650644683,115.54069767441861,653.6325441436559,163.975,694.1544836832496,178C734.6764232228434,192.025,815.720302302031,239.2,856.2422418416247,239.2C896.7641813812186,239.2,977.8080604604061,193.3,1018.3299999999999,178L1018.3299999999999,331L45.359375,331Z"
                                    fill-opacity="0.4"
                                    style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 0.4;"></path>
                                <path fill="none" stroke="#01c0c8"
                                    d="M45.359375,331C85.88131453959379,305.5,166.92519361878135,236.65,207.44713315837515,229C247.96907269796895,221.35,329.0129517771565,282.5325581395349,369.5348913167503,269.8C410.16784986878133,257.0325581395349,491.4337669728435,138.4906976744186,532.0667255248745,127C572.5886650644683,115.54069767441861,653.6325441436559,163.975,694.1544836832496,178C734.6764232228434,192.025,815.720302302031,239.2,856.2422418416247,239.2C896.7641813812186,239.2,977.8080604604061,193.3,1018.3299999999999,178"
                                    stroke-width="0" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path>
                                <circle cx="45.359375" cy="331" r="0" fill="#01c0c8" stroke="#01c0c8" stroke-width="1"
                                    style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                <circle cx="207.44713315837515" cy="229" r="0" fill="#01c0c8" stroke="#01c0c8"
                                    stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                <circle cx="369.5348913167503" cy="269.8" r="0" fill="#01c0c8" stroke="#01c0c8"
                                    stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                <circle cx="532.0667255248745" cy="127" r="0" fill="#01c0c8" stroke="#01c0c8"
                                    stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                <circle cx="694.1544836832496" cy="178" r="0" fill="#01c0c8" stroke="#01c0c8"
                                    stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                <circle cx="856.2422418416247" cy="239.2" r="0" fill="#01c0c8" stroke="#01c0c8"
                                    stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                                <circle cx="1018.3299999999999" cy="178" r="0" fill="#01c0c8" stroke="#01c0c8"
                                    stroke-width="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></circle>
                            </svg>
                            <div class="morris-hover morris-default-style" style="display: none;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- Total Leads -->
        <!-- ============================================================== -->
        <!-- .row -->
        <div class="row">
            <div class="col-md-4 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title"><small class="pull-right text-success"><i class="fa fa-sort-asc"></i> 18%
                                High then last month</small> Total Leads</h5>
                        <div class="stats-row">
                            <div class="stat-item">
                                <h6>Overall Growth</h6>
                                <b>80.40%</b>
                            </div>
                            <div class="stat-item">
                                <h6>Montly</h6>
                                <b>15.40%</b>
                            </div>
                            <div class="stat-item">
                                <h6>Day</h6>
                                <b>5.50%</b>
                            </div>
                        </div>
                    </div>
                    <div id="sparkline8" class="minus-mar"><canvas width="531" height="130"
                            style="display: inline-block; width: 531.656px; height: 130px; vertical-align: top;"></canvas>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title"><small class="pull-right text-danger"><i class="fa fa-sort-desc"></i> 18%
                                High then last month</small> Total Vendor</h5>
                        <div class="stats-row">
                            <div class="stat-item">
                                <h6>Overall Growth</h6>
                                <b>80.40%</b>
                            </div>
                            <div class="stat-item">
                                <h6>Montly</h6>
                                <b>15.40%</b>
                            </div>
                            <div class="stat-item">
                                <h6>Day</h6>
                                <b>5.50%</b>
                            </div>
                        </div>
                    </div>
                    <div id="sparkline9" class="minus-mar"><canvas width="531" height="130"
                            style="display: inline-block; width: 531.656px; height: 130px; vertical-align: top;"></canvas>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title"><small class="pull-right text-success"><i class="fa fa-sort-asc"></i> 18%
                                High then last month</small> Invoice</h5>
                        <div class="stats-row">
                            <div class="stat-item">
                                <h6>Overall Growth</h6>
                                <b>80.40%</b>
                            </div>
                            <div class="stat-item">
                                <h6>Montly</h6>
                                <b>15.40%</b>
                            </div>
                            <div class="stat-item">
                                <h6>Day</h6>
                                <b>5.50%</b>
                            </div>
                        </div>
                    </div>
                    <div id="sparkline10" class="minus-mar"><canvas width="531" height="130"
                            style="display: inline-block; width: 531.656px; height: 130px; vertical-align: top;"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
        <!-- ============================================================== -->
        <!-- End Total Leads -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- New Customers List and New Products List -->
        <!-- ============================================================== -->
        <!-- /row -->
        <div class="row">
            <div class="col-sm-6">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title m-b-0">New Customers List</h5>
                        <p class="text-muted">this is the sample data here for crm</p>
                        <div class="table-responsive">
                            <table class="table mb-0">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Username</th>
                                        <th>Role</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>Deshmukh</td>
                                        <td>Prohaska</td>
                                        <td>@Genelia</td>
                                        <td><span class="label label-danger">admin</span> </td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Deshmukh</td>
                                        <td>Gaylord</td>
                                        <td>@Ritesh</td>
                                        <td><span class="label label-info">member</span> </td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>Sanghani</td>
                                        <td>Gusikowski</td>
                                        <td>@Govinda</td>
                                        <td><span class="label label-warning">developer</span> </td>
                                    </tr>
                                    <tr>
                                        <td>4</td>
                                        <td>Roshan</td>
                                        <td>Rogahn</td>
                                        <td>@Hritik</td>
                                        <td><span class="label label-success">supporter</span> </td>
                                    </tr>
                                    <tr>
                                        <td>5</td>
                                        <td>Joshi</td>
                                        <td>Hickle</td>
                                        <td>@Maruti</td>
                                        <td><span class="label label-info">member</span> </td>
                                    </tr>
                                    <tr>
                                        <td class="pb-0">6</td>
                                        <td class="pb-0">Nigam</td>
                                        <td class="pb-0">Eichmann</td>
                                        <td class="pb-0">@Sonu</td>
                                        <td class="pb-0"><span class="label label-success">supporter</span> </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title m-b-0">New Product List</h5>
                        <p class="text-muted">this is the sample data here for crm</p>
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Products</th>
                                        <th>Popularity</th>
                                        <th>Sales</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>Milk Powder</td>
                                        <td><span class="peity-line" data-width="120"
                                                data-peity="{ &quot;fill&quot;: [&quot;#13dafe&quot;], &quot;stroke&quot;:[&quot;#13dafe&quot;]}"
                                                data-height="40"
                                                style="display: none;">0,-3,-2,-4,-5,-4,-3,-2,-5,-1</span><svg
                                                class="peity" height="40" width="120">
                                                <polygon fill="#13dafe"
                                                    points="0 0.5 0 0.5 13.333333333333334 23.9 26.666666666666668 16.1 40 31.7 53.333333333333336 39.5 66.66666666666667 31.7 80 23.9 93.33333333333334 16.1 106.66666666666667 39.5 120 8.299999999999997 120 0.5">
                                                </polygon>
                                                <polyline fill="none"
                                                    points="0 0.5 13.333333333333334 23.9 26.666666666666668 16.1 40 31.7 53.333333333333336 39.5 66.66666666666667 31.7 80 23.9 93.33333333333334 16.1 106.66666666666667 39.5 120 8.299999999999997"
                                                    stroke="#13dafe" stroke-width="1" stroke-linecap="square">
                                                </polyline>
                                            </svg> </td>
                                        <td><span class="text-danger text-semibold"><i class="fa fa-level-down"
                                                    aria-hidden="true"></i> 28.76%</span> </td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Air Conditioner</td>
                                        <td><span class="peity-line" data-width="120"
                                                data-peity="{ &quot;fill&quot;: [&quot;#13dafe&quot;], &quot;stroke&quot;:[&quot;#13dafe&quot;]}"
                                                data-height="40"
                                                style="display: none;">0,-1,-1,-2,-3,-1,-2,-3,-1,-2</span><svg
                                                class="peity" height="40" width="120">
                                                <polygon fill="#13dafe"
                                                    points="0 0.5 0 0.5 13.333333333333334 13.5 26.666666666666668 13.5 40 26.5 53.333333333333336 39.5 66.66666666666667 13.5 80 26.5 93.33333333333334 39.5 106.66666666666667 13.5 120 26.5 120 0.5">
                                                </polygon>
                                                <polyline fill="none"
                                                    points="0 0.5 13.333333333333334 13.5 26.666666666666668 13.5 40 26.5 53.333333333333336 39.5 66.66666666666667 13.5 80 26.5 93.33333333333334 39.5 106.66666666666667 13.5 120 26.5"
                                                    stroke="#13dafe" stroke-width="1" stroke-linecap="square">
                                                </polyline>
                                            </svg> </td>
                                        <td><span class="text-warning text-semibold"><i class="fa fa-level-down"
                                                    aria-hidden="true"></i> 8.55%</span> </td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>RC Cars</td>
                                        <td><span class="peity-line" data-width="120"
                                                data-peity="{ &quot;fill&quot;: [&quot;#13dafe&quot;], &quot;stroke&quot;:[&quot;#13dafe&quot;]}"
                                                data-height="40" style="display: none;">0,3,6,1,2,4,6,3,2,1</span><svg
                                                class="peity" height="40" width="120">
                                                <polygon fill="#13dafe"
                                                    points="0 39.5 0 39.5 13.333333333333334 20 26.666666666666668 0.5 40 33 53.333333333333336 26.5 66.66666666666667 13.5 80 0.5 93.33333333333334 20 106.66666666666667 26.5 120 33 120 39.5">
                                                </polygon>
                                                <polyline fill="none"
                                                    points="0 39.5 13.333333333333334 20 26.666666666666668 0.5 40 33 53.333333333333336 26.5 66.66666666666667 13.5 80 0.5 93.33333333333334 20 106.66666666666667 26.5 120 33"
                                                    stroke="#13dafe" stroke-width="1" stroke-linecap="square">
                                                </polyline>
                                            </svg> </td>
                                        <td><span class="text-success text-semibold"><i class="fa fa-level-up"
                                                    aria-hidden="true"></i> 58.56%</span> </td>
                                    </tr>
                                    <tr>
                                        <td>4</td>
                                        <td>Down Coat</td>
                                        <td><span class="peity-line" data-width="120"
                                                data-peity="{ &quot;fill&quot;: [&quot;#13dafe&quot;], &quot;stroke&quot;:[&quot;#13dafe&quot;]}"
                                                data-height="40" style="display: none;">0,3,6,4,5,4,7,3,4,2</span><svg
                                                class="peity" height="40" width="120">
                                                <polygon fill="#13dafe"
                                                    points="0 39.5 0 39.5 13.333333333333334 22.78571428571429 26.666666666666668 6.0714285714285765 40 17.214285714285715 53.333333333333336 11.642857142857142 66.66666666666667 17.214285714285715 80 0.5 93.33333333333334 22.78571428571429 106.66666666666667 17.214285714285715 120 28.357142857142858 120 39.5">
                                                </polygon>
                                                <polyline fill="none"
                                                    points="0 39.5 13.333333333333334 22.78571428571429 26.666666666666668 6.0714285714285765 40 17.214285714285715 53.333333333333336 11.642857142857142 66.66666666666667 17.214285714285715 80 0.5 93.33333333333334 22.78571428571429 106.66666666666667 17.214285714285715 120 28.357142857142858"
                                                    stroke="#13dafe" stroke-width="1" stroke-linecap="square">
                                                </polyline>
                                            </svg> </td>
                                        <td><span class="text-info text-semibold"><i class="fa fa-level-up"
                                                    aria-hidden="true"></i> 35.76%</span> </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row -->
        <!-- ============================================================== -->
        <!-- End Page Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right sidebar -->
        <!-- ============================================================== -->
        <!-- .right-sidebar -->
        <div class="right-sidebar ps ps--theme_default" data-ps-id="76936b6a-8b85-26c0-d7ea-5bf21b11a7d4">
            <div class="slimscrollright">
                <div class="rpanel-title"> Service Panel <span><i class="ti-close right-side-toggle"></i></span> </div>
                <div class="r-panel-body">
                    <ul id="themecolors" class="m-t-20">
                        <li><b>With Light sidebar</b></li>
                        <li><a href="javascript:void(0)" data-skin="skin-default" class="default-theme">1</a></li>
                        <li><a href="javascript:void(0)" data-skin="skin-green" class="green-theme">2</a></li>
                        <li><a href="javascript:void(0)" data-skin="skin-red" class="red-theme">3</a></li>
                        <li><a href="javascript:void(0)" data-skin="skin-blue" class="blue-theme">4</a></li>
                        <li><a href="javascript:void(0)" data-skin="skin-purple" class="purple-theme">5</a></li>
                        <li><a href="javascript:void(0)" data-skin="skin-megna" class="megna-theme">6</a></li>
                        <li class="d-block m-t-30"><b>With Dark sidebar</b></li>
                        <li><a href="javascript:void(0)" data-skin="skin-default-dark"
                                class="default-dark-theme working">7</a></li>
                        <li><a href="javascript:void(0)" data-skin="skin-green-dark" class="green-dark-theme">8</a></li>
                        <li><a href="javascript:void(0)" data-skin="skin-red-dark" class="red-dark-theme">9</a></li>
                        <li><a href="javascript:void(0)" data-skin="skin-blue-dark" class="blue-dark-theme">10</a></li>
                        <li><a href="javascript:void(0)" data-skin="skin-purple-dark" class="purple-dark-theme">11</a>
                        </li>
                        <li><a href="javascript:void(0)" data-skin="skin-megna-dark" class="megna-dark-theme ">12</a>
                        </li>
                    </ul>
                    <ul class="m-t-20 chatonline">
                        <li><b>Chat option</b></li>
                        <li>
                            <a href="javascript:void(0)"><img src="../assets/images/users/1.jpg" alt="user-img"
                                    class="img-circle"> <span>Varun Dhavan <small
                                        class="text-success">online</small></span></a>
                        </li>
                        <li>
                            <a href="javascript:void(0)"><img src="../assets/images/users/2.jpg" alt="user-img"
                                    class="img-circle"> <span>Genelia Deshmukh <small
                                        class="text-warning">Away</small></span></a>
                        </li>
                        <li>
                            <a href="javascript:void(0)"><img src="../assets/images/users/3.jpg" alt="user-img"
                                    class="img-circle"> <span>Ritesh Deshmukh <small
                                        class="text-danger">Busy</small></span></a>
                        </li>
                        <li>
                            <a href="javascript:void(0)"><img src="../assets/images/users/4.jpg" alt="user-img"
                                    class="img-circle"> <span>Arijit Sinh <small
                                        class="text-muted">Offline</small></span></a>
                        </li>
                        <li>
                            <a href="javascript:void(0)"><img src="../assets/images/users/5.jpg" alt="user-img"
                                    class="img-circle"> <span>Govinda Star <small
                                        class="text-success">online</small></span></a>
                        </li>
                        <li>
                            <a href="javascript:void(0)"><img src="../assets/images/users/6.jpg" alt="user-img"
                                    class="img-circle"> <span>John Abraham<small
                                        class="text-success">online</small></span></a>
                        </li>
                        <li>
                            <a href="javascript:void(0)"><img src="../assets/images/users/7.jpg" alt="user-img"
                                    class="img-circle"> <span>Hritik Roshan<small
                                        class="text-success">online</small></span></a>
                        </li>
                        <li>
                            <a href="javascript:void(0)"><img src="../assets/images/users/8.jpg" alt="user-img"
                                    class="img-circle"> <span>Pwandeep rajan <small
                                        class="text-success">online</small></span></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="ps__scrollbar-x-rail" style="left: 0px; bottom: 0px;">
                <div class="ps__scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div>
            </div>
            <div class="ps__scrollbar-y-rail" style="top: 0px; right: 0px;">
                <div class="ps__scrollbar-y" tabindex="0" style="top: 0px; height: 0px;"></div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Right sidebar -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->
</div>
@endsection