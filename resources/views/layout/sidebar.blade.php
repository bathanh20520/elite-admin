<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar ps ps--theme_default ps--active-y" data-ps-id="c86d4159-f971-a10c-c2d2-d334c026b899">
        <!-- User Profile-->
        <div class="user-profile">
            <div class="user-pro-body">
                <div><img src="{{asset('images/2.jpg')}}" alt="user-img" class="img-circle"></div>
                <div class="dropdown">
                    <a href="javascript:void(0)" class="dropdown-toggle u-dropdown link hide-menu"
                        data-bs-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Steave
                        Gection <span class="caret"></span></a>
                    <div class="dropdown-menu animated flipInY">
                        <!-- text-->
                        <a href="javascript:void(0)" class="dropdown-item"><i class="ti-user"></i> My Profile</a>
                        <!-- text-->
                        <a href="javascript:void(0)" class="dropdown-item"><i class="ti-wallet"></i> My Balance</a>
                        <!-- text-->
                        <a href="javascript:void(0)" class="dropdown-item"><i class="ti-email"></i> Inbox</a>
                        <!-- text-->
                        <div class="dropdown-divider"></div>
                        <!-- text-->
                        <a href="javascript:void(0)" class="dropdown-item"><i class="ti-settings"></i> Account
                            Setting</a>
                        <!-- text-->
                        <div class="dropdown-divider"></div>
                        <!-- text-->
                        <a href="pages-login.html" class="dropdown-item"><i class="fas fa-power-off"></i> Logout</a>
                        <!-- text-->
                    </div>
                </div>
            </div>
        </div>
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav active">
            <ul id="sidebarnav" class="in">
                <li class="nav-small-cap">--- MAIN MENU</li>
                <li class="active">
                    <a class="waves-effect waves-dark active" href="{{route('get.admin.dashboard')}}" aria-expanded="false">
                        <i class="icon-speedometer"></i>
                        <span class="hide-menu">Dashboard</span>
                    </a>
                </li>
                <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i
                            class="icon-equalizer"></i><span class="hide-menu">Ui Elements </span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{route('get.admin.elements')}}">Cards</a></li>
                    </ul>
                </li>
                <li class="nav-small-cap">--- SUPPORTS</li>
                <li>
                    <a class="waves-effect waves-dark" href="pages-login.html" aria-expanded="false">
                        <i class="icon-logout"></i>
                        <span class="hide-menu">Log Out</span>
                    </a>
                </li>
                </li>
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
        <div class="ps__scrollbar-x-rail" style="left: 0px; bottom: 0px;">
            <div class="ps__scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div>
        </div>
        <div class="ps__scrollbar-y-rail" style="top: 0px; height: 444px; right: 0px;">
            <div class="ps__scrollbar-y" tabindex="0" style="top: 0px; height: 173px;"></div>
        </div>
    </div>
    <!-- End Sidebar scroll-->
</aside>